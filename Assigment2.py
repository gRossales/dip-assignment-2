# Gabriel R. do P. Rossales 6608843
# SCC0251 2020_1
# Image Enhancement and Filtering
#https://gitlab.com/gRossales/dip-assignment-2

import numpy as np
import imageio


# Performs normalization to 0-255
# Params:
#   img: ndarray - 2d image
def normalize(img):
    return (img - np.amin(img)) * 255 / (np.amax(img)-np.amin(img))


# Perform gaussian filter
# Params:
#   x: float - euclidean distance from origin
#   sigma: float - standard deviation of gaussian distribution
def gaussianKernel(x, sigma):
    return np.exp(-(x ** 2) / (2 * (sigma ** 2)))/ (2 * np.pi * (sigma ** 2))


# Perform bilateral filter
# Params:
#   img: ndarray - 2d image
#   sigma_row: float - standard deviation of gaussian filter for columns
#   sigma_col: float - standard deviation of gaussian filter for columns
def bilateralFilter(img, n, sigma_s, sigma_r):
    spatial_component = np.array(
        [[gaussianKernel(np.linalg.norm((i, j)), sigma_s) for j in range(1 + (-n // 2), 1 + n // 2)] for i in
         range(1 + (-n // 2), 1 + n // 2)])

    if n % 2 == 1:
        padded = np.pad(img, n // 2, 'constant')
        center = n // 2
    else:
        padded = np.pad(img, ((n // 2 - 1, n // 2), (n // 2 - 1, n // 2)), 'constant')
        center = n // 2 - 1
    I = []
    for i in range(padded.shape[0] - (n-1)):
        for j in range(padded.shape[1] - (n-1)):
            subMatrix = padded[i:i+n, j:j+n]
            gs = spatial_component
            gr = np.array([gaussianKernel(x, sigma_r) for x in (subMatrix-np.ones((n, n))*subMatrix[center, center])])
            w = gs*gr
            I.append(np.sum(subMatrix*w)/np.sum(w))
    return np.array(I).reshape(img.shape)


# Perform unsharpen mask
# Params:
#   img: ndarray - 2d image
#   c: float - filter weight
#   kernel: ndarray - 3x3
def unsharpenMask(img, c, kernel):
    padded = np.pad(img, 1, 'constant')
    l = []
    for i in range(padded.shape[0] - 2):
        for j in range(padded.shape[1] - 2):
            l.append(np.sum(padded[i:i + 3, j:j + 3] * kernel))
    convoluted = np.array(l).reshape(img.shape)
    scaled = normalize(convoluted)
    added = c * scaled + img
    return normalize(added)


# Perform vignette
# Params:
#   img: ndarray - 2d image
#   sigma_row: float - standard deviation of gaussian filter for rows
#   sigma_col: float - standard deviation of gaussian filter for columns
def vignette(img, sigma_row, sigma_col):
    w_row = np.array([gaussianKernel(x, sigma_row) for x in range(1 + (-img.shape[0] // 2), 1 + (img.shape[0] // 2))])
    w_col = np.array([gaussianKernel(x, sigma_col) for x in range(1 + (-img.shape[1] // 2), 1 + (img.shape[1] // 2))])
    kernel = w_col * w_row.reshape(img.shape[0], 1)
    filtered = kernel * img
    return normalize(filtered)


# Perform root squared error calculation
# Params:
#   m - ndarray - 2d image - modified image
#   r - ndarray - 2d image - reference image
def RSE(m, r):
    err = np.sqrt(np.sum(np.square(m.astype(float) - r.astype(float))))
    return err


# Read inputs
filename = str(input()).rstrip()
input_img = imageio.imread(filename)
method = int(input())
save = int(input())

if method == 1:
    n = int(input())
    sigma_s = float(input())
    sigma_r = float(input())
    output_img = bilateralFilter(input_img, n, sigma_s, sigma_r)

if method == 2:
    c = float(input())
    kernel_option = int(input())
    kernel = np.array([[0, -1, 0], [-1, 4, -1], [0, -1, 0]]) if kernel_option == 1 else np.array(
        [[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])
    output_img = unsharpenMask(input_img, c, kernel)

if method == 3:
    sigma_row = float(input())
    sigma_col = float(input())
    output_img = vignette(input_img, sigma_row, sigma_col)

print(np.round(RSE(output_img, input_img), 4))

if save == 1:
    imageio.imwrite('output_img.png', output_img)
