## DIP Assignment 2 
Image Processing - SCC0251

Assignement 2 - Image Enhancement and Filtering 

Author: Gabriel R. do P. Rossales

Folder and files:

- [Python code](Assigment2.py) is the python script submitted to run.codes
- [Images](./images) contain images used in demos
- [Notebook with Demo](dip02-filtering.ipynb) is a notebook exemplifying functions developed and submitted
